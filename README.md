# PSI3 - REST klient

Task was to create script that uses ISS Rest client to determine where ISS is right now and if there are 
good condtitions for observing it

# BUILD
All the neccesary code is written in main.py script run by command
```bash
python main.py
```
or
```bash
python3 main.py
```
depending on your operating system and version of python.

# SCRIPT DESCRIPTION

Script firstly gets json response from ISS Rest API and finds its longitude and latitude.

With this information it can determine sunset and sunrise at the place its currently flying over 
and with gained timestamp from ISS API data script determines if there are good observing conditions.


