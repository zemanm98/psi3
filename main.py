import requests
import json
from datetime import datetime

# getting response from iss api
response = requests.get("http://api.open-notify.org/iss-now.json")
iss_data = json.loads(response.text)

# getting iss longitude and latitude
iss_longitude = iss_data['iss_position']['longitude']
iss_latitude = iss_data['iss_position']['latitude']

# getting and parsing timestamp from iss response
iss_timestamp = requests.get(
    'https://showcase.api.linx.twenty57.net/UnixTime/fromunix?timestamp=' + str(iss_data["timestamp"])).json()
iss_time = str(datetime.strptime(iss_timestamp.split()[1], '%H:%M:%S')).split()[1]

# getting sunset and sunrise data from iss longitude and latitude
response = requests.get("https://api.sunrise-sunset.org/json?lat=" + str(iss_latitude) + "&lng=" + str(iss_longitude))
sunset_data = response.json()

# parsing sunset and sunrise to datetime object
sunrise = str(sunset_data['results']['sunrise'])
in_time = datetime.strptime(sunrise, "%I:%M:%S %p")
sunrise_time = datetime.strftime(in_time, "%H:%M:%S")
sunset = str(sunset_data['results']['sunset'])
in_time = datetime.strptime(sunset, "%I:%M:%S %p")
sunset_time = datetime.strftime(in_time, "%H:%M:%S")

iss_time_parsed = int(iss_time.replace(":", ""))
sunrise_time_parsed = int(sunrise_time.replace(":", ""))
sunset_time_parsed = int(sunset_time.replace(":", ""))

print("ISS se nachazi na zemepisne sirce: " + str(iss_latitude) + " a zemepisne delce: " + str(iss_longitude))

# determining iss position to day or night at the place its situated at
if iss_time_parsed < sunrise_time_parsed or iss_time_parsed > sunset_time_parsed:
    print("ISS je prave na neosvetlene strane Zeme")
else:
    print("ISS je prave na osvetlene strane Zeme")

before_sunrise = sunrise_time_parsed - iss_time_parsed
after_sunset = iss_time_parsed - sunset_time_parsed

# determining if there are good conditions for observing iss at the place it can be seen right now.
if before_sunrise > 0:
    if 20000 >= before_sunrise >= 10000:
        print("Jsou dobre podminky pro pozorovani (1-2 hodiny pred vychodem slunce")
    else:
        print("Nejsou dobre podminky pro pozorovani")
elif after_sunset > 0:
    if 20000 >= after_sunset >= 10000:
        print("Jsou dobre podminky pro pozorovani (1-2 hodiny po zapadu slunce)")
    else:
        print("Nejsou dobre podminky pro pozorovani")
else:
    print("Nejsou dobre podminky pro pozorovani")
